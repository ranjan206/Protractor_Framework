
var web_common = require('D:/Protractor_Workspace/Protractor_Framework/PageObject_Repo/Web_Common');

var Customers = function() {
	var btn_process = element(by.xpath("//button[contains(text(),'Process')]"));
	var fname = element(by.xpath("//td[text()='Harry']")); 
  
  this.verify_Customers_Details = function(fName){
	  
	  if(expect(fname.isPresent()).toBe(true))
	     { 
		  fname.getText().then(function(fName){ 
	      console.log("First name is as expected: " + fName); 
	     }); 
	   } 
	   else 
	   {
	    console.log("First name is not as expected") ;
	   }
	  console.log("Account created") ;
	  return fname.getText() ;
  };
  
};
module.exports = new Customers();