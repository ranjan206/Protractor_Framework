
var reporter = require('D:/Protractor_Workspace/Protractor_Framework/coreframework/Reporter');
var web_utils = require('D:/Protractor_Workspace/Protractor_Framework/coreframework/Web_utils');

var Web_Common = function() {

	var btn_Home = element(by.className('btn home'));
	
  this.goBacktoHomePage = function(){
	  web_utils.clickOnElement(btn_Home) ;
	  //btn_Home.click() ;
	  console.log("Go back to homepage") ;
  } ;
  
  this.acceptAlert = function(alertUrl) {
	/*//	browser.ignoreSynchronization = true ;
	//	browser.waitForAngularEnabled(false)
	//	browser.get(alertUrl);
	  
	  var EC = protractor.ExpectedConditions;
	  browser.wait(EC.alertIsPresent(), 5000);
	  browser.get(alertUrl);
	  
	  browser.getCurrentUrl().then( function( url ) {
		  console.log(url);
		  });
	  
	  console.log("Accepted alert message successfully:  "+alertUrl+" \n --- \n"+browser.getCurrentUrl()) ;
	  
	  
	//  browser.refresh();
	//  browser.wait(protractor.ExpectedConditions.alertIsPresent(), 1000);
	  browser.switchTo().alert().accept();
	  */
	  
	  browser.get(alertUrl);
	  browser.pause() ;
	  browser.switchTo().alert().accept() ;
	  debugger ;
		//console.log("Accepted alert message successfully:  "+alertUrl+" \n --- \n"+browser.getCurrentUrl()) ;
	} ;
  
  
  this.generateRandomText = function(){
	  var text = "";
	  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

	  for (var i = 0; i < 5; i++)
	    text += possible.charAt(Math.floor(Math.random() * possible.length));
	  return text;
  }
};

module.exports = new Web_Common();