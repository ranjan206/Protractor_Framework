
var reporter = require('D:/Protractor_Workspace/Protractor_Framework/coreframework/Reporter');
var web_utils = require('D:/Protractor_Workspace/Protractor_Framework/coreframework/Web_utils');

var AngularHomepage = function() {

	var title_Bank = element(by.css('strong.mainHeading'));
	var btn_Home = element(by.className('btn home'));
	var btn_ManagerLogin = element(by.css("button[ng-click *= 'manager']"));
	var btn_CustomerLogin = element(by.css("button[ng-click *= 'customer']"));
	var btn_AddCustomer = element(by.xpath("//button[contains(text(),'Add Customer')]"));
	
  this.get = function() {
	 browser.get(browser.params.url);  
	 browser.manage().window().maximize();
     //expect(btn_Home.isDisplayed());
	 
	 if (web_utils.isElementPresent(btn_Home)) {
		 reporter.logteststeps('Bank application launched successfully',true) ;
	} else {
		reporter.logteststeps('Failed to launch bank application',true) ;
	} ;
	 
    return title_Bank.getText();
  };
  
  this.clickOnManagerLogin = function(){
/*	  btn_ManagerLogin.click() ;
	  console.log("Manager login button clicked") ;
*/	  
	  
	  web_utils.clickOnElement(btn_ManagerLogin) ;
	  browser.waitForAngular();
	  reporter.logteststeps('Manager login button clicked',false) ;
	  
	  
  } ;
  
  this.clickOnCustomerLogin = function(){
	  btn_CustomerLogin.click() ;
	  browser.waitForAngular();
	  browser.waitForAngular();
	  console.log("Customer login button clicked") ;
  } ;
  
  this.clickOnAddCustomer = function(){
	  btn_AddCustomer.click() ;
	  console.log("Add customer button clicked") ;
  } ;
};
module.exports = new AngularHomepage();