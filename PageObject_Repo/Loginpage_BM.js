
var reporter = require('D:/Protractor_Workspace/Protractor_Framework/coreframework/Reporter');
var web_utils = require('D:/Protractor_Workspace/Protractor_Framework/coreframework/Web_utils');

var Loginpage_BM = function() {
 // var nameInput = element(by.model('yourName'));
 // var greeting = element(by.binding('yourName'));

	var title_Bank = element(by.css('strong.mainHeading'));
	var btn_Home = element(by.className('btn home'));
	var btn_ManagerLogin = element(by.css("button[ng-click *= 'manager']"));
	//var btn_AddCustomer = element(by.css("button[ng-click *= 'addCust']"));
	var btn_AddCustomer = element(by.xpath("//button[contains(text(),'Add Customer')]"));
	var btn_OpenAcc = element(by.xpath("//button[contains(text(),'Open Account')]"));
	var btn_Customer = element(by.xpath("//button[contains(text(),'Customers')]"));
	
	
  this.clickOnAddCustomer = async function(){
	 /* btn_AddCustomer.click() ;
	  console.log("Add customer button clicked") ;*/
	  
	  web_utils.clickOnElement(btn_AddCustomer) ;
	  reporter.logteststeps('Add customer button clicked',true) ;
  } ;
  
  this.clickOnOpenAcc = async function(){
	  /*btn_OpenAcc.click() ;
	  console.log("Open Account button clicked") ;
	  */
	  web_utils.clickOnElement(btn_OpenAcc) ;
	  reporter.logteststeps('Open Account button clicked',true) ;
  } ;
  
  this.clickOnCustomer = async function(){
	 /* btn_Customer.click() ;
	  console.log("Cutomersr button clicked") ;
	  */
	  web_utils.clickOnElement(btn_Customer) ;
	  reporter.logteststeps('Cutomersr button clicked',true) ;
  } ;
};
module.exports = new Loginpage_BM();