var Homepage = function() {
	
	  var title_Bank = element(by.css('strong.mainHeading'));
	  var btn_Home = element(by.className('btn home'));
	  var btn_ManagerLogin = element(by.css("button[ng-click *= 'manager']"));
	  
	  this.get = async function() {
		  //currently we are getting this from .js directly but we can get it from the property file
		    await browser.get('http://www.globalsqa.com/angularJs-protractor/BankingProject/#/login');
		    browser.manage().window().maximize();
		    var title_val = title_Bank.getText();
		    expect(title_val.toEqual('XYZ Bank'));
		    expect(btn_Home.isDisplayed());
	  };
};
module.exports = new HomePage();