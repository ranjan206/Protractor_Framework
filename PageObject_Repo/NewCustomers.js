
var web_common = require('D:/Protractor_Workspace/Protractor_Framework/PageObject_Repo/Web_Common');
var reporter = require('D:/Protractor_Workspace/Protractor_Framework/coreframework/Reporter');
var web_utils = require('D:/Protractor_Workspace/Protractor_Framework/coreframework/Web_utils');

var NewCustomers = function() {
  var btn_AddCustomer = element(by.xpath("//button[contains(text(),'Add Customer')]"));
  var field_FN = element(by.model('fName'));
  var field_LN = element(by.model('lName'));
  var field_PC = element(by.model('postCd'));
  var btn_AddCust = element(by.xpath("//form//button[contains(text(),'Add Customer')]"));
  
  this.fillCutomerDetails = function(){
	  
	 // console.log(web_common.generateRandomText()) ;
	  var fName = web_common.generateRandomText() ;
	  var sName = web_common.generateRandomText() ;
	  var pCode = web_common.generateRandomText() ;
	  /*
	  field_FN.sendKeys(fName) ;
	  field_LN.sendKeys(sName) ;
	  field_PC.sendKeys(pCode) ;*/
	  
	  web_utils.sendkeysByValue(field_FN, fName) ;
	  web_utils.sendkeysByValue(field_LN, sName) ;
	  web_utils.sendkeysByValue(field_PC, pCode) ;
	  
	 // btn_AddCust.click() ;
	  
	  reporter.logteststeps("Filled with customer details \n\n Fname: "+fName+" \n\n SName: "+sName+" \n\n Postal code: "+pCode,true) ;
	  
	  console.log("Filled with customer details \n Fname: "+fName+" \n SName: "+sName+" \n Postal code: "+pCode) ;
	  
	  //web_common.acceptAlert('http://www.globalsqa.com') ;
	  
  };
};
module.exports = new NewCustomers();