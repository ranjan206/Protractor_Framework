var web_common = require('D:/Protractor_Workspace/Protractor_Framework/PageObject_Repo/Web_Common');
var reporter = require('D:/Protractor_Workspace/Protractor_Framework/coreframework/Reporter');
var web_utils = require('D:/Protractor_Workspace/Protractor_Framework/coreframework/Web_utils');


var Customer_loginpage = function() {

	var btn_login = element(by.xpath("//button[contains(text(),'Login')]"));
	var userList = element(by.model("custId"));
	var loggednUN = element(by.xpath("//strong[contains(text(),'Welcome')]//span"));

	//Deposit amount..
	var Balance_Amount = element(by.xpath("(//div[contains(text(),'Account Number')]//following-sibling::strong[2])[1]"));
	var btn_Deposite = element(by.cssContainingText("button[ng-class = 'btnClass2']",'Deposit')) ;
	var AmountTBD = element(by.model('amount')) ;
	var btn_Submit_Dp = element(by.xpath("//button[contains(text(),'Deposit')][@type = 'submit']"));
	var successful_Msg_Dp = element(by.xpath("//span[contains(text(),'Deposit Successful')]"));
	
	
	//Withdraw amount..
	var btn_Withdrawl = element(by.cssContainingText("button[ng-class = 'btnClass3']",'Withdrawl')) ;
	var AmountWithdrawl = element(by.model('amount')) ;
	var btn_Submit_Wd = element(by.xpath("//button[contains(text(),'Withdraw')][@type = 'submit']"));
	var successful_Msg_Wd = element(by.xpath("//span[contains(text(),'Transaction successful')]"));
	
	
	this.loginascustomer = function() {
		
		element(by.cssContainingText("#userSelect option[ng-repeat *= 'cust in Customers']", browser.params.accountholder_name)).click();
		console.log("Logged in as : "+browser.params.accountholder_name);
		
		browser.waitForAngular();
		//if (expect(btn_login.isDisplayed())) {
		if(web_utils.isElementPresent(btn_login)){
			//btn_login.click();
			web_utils.clickOnElement(btn_login) ;
		}
		if (expect(loggednUN.getText()).toBe(browser.params.accountholder_name)) {
			//console.log("Logged in successfully");
			reporter.logteststeps('Logged in successfully',true) ;
		}
		else {
			//console.log("Log in failed");
			reporter.logteststeps('Log in failed',true) ;
		}
	};
	
	
	this.depositeMoney = function() {
		
		/*btn_Deposite.click() ;
		AmountTBD.sendKeys(browser.params.amountToDeposit) ;
		btn_Submit_Dp.click() ;
*/
		web_utils.clickOnElement(btn_Deposite);
		web_utils.sendkeysByValue(AmountTBD, browser.params.amountToDeposit) ;
		web_utils.clickOnElement(btn_Submit_Dp) ;
		
		if (expect(successful_Msg_Dp.getText()).toBe("Deposit Successful")) {
			//console.log("Amount deposited is successfull:"+browser.params.amountToDeposit) ;	
			reporter.logteststeps("Amount deposited is successfull: "+browser.params.amountToDeposit,true) ;
			
		} else{
			//console.log("Deposit failed") ;
			reporter.logteststeps("Deposit failed",true) ;
		}
		
	};
	
	this.withdrawmoney = function() {
		
		/*btn_Withdrawl.click() ;
		AmountWithdrawl.sendKeys(browser.params.withdrawAmount) ;
		btn_Submit_Wd.click() ;*/
		
		web_utils.clickOnElement(btn_Withdrawl);
		web_utils.sendkeysByValue(AmountWithdrawl, browser.params.withdrawAmount) ;
		web_utils.clickOnElement(btn_Submit_Wd) ;
		
		
		if (expect(successful_Msg_Wd.getText()).toBe("Transaction successful")) {
			//console.log("Amount withdrawl is successful:"+browser.params.withdrawAmount) ;
			
			reporter.logteststeps("Amount withdrawl is successful:"+browser.params.withdrawAmount,true) ;
		} else{
			//console.log("Withdrawl failed") ;
			reporter.logteststeps("Withdrawl failed",true) ;
		}
	};
	
	/*this.verifyTrasaction = function() {
		
	};*/

};
module.exports = new Customer_loginpage();