var dataPoints = [];
 
$.get("https://canvasjs.com/services/data/datapoints.php?xstart=5&ystart=10&length=10&type=xml", function(data) {
	$(data).find("point").each(function () {
		var $dataPoint = $(this);
		var x = $dataPoint.find("x").text();
		var y = $dataPoint.find("y").text();
		dataPoints.push({x: parseFloat(x), y: parseFloat(y)});
	});
		
	var chart = new CanvasJS.Chart("chartContainer", {
		title: {
		    text: "Chart Using XML Data",
		},
		data: [{
		     type: "column",
		     dataPoints: dataPoints,
		  }]
	});
	
	chart.render();
 