var angularHomepage = require('D:/Protractor_Workspace/Protractor_Framework/PageObject_Repo/AngularHomepage');
var loginpage_BM = require('D:/Protractor_Workspace/Protractor_Framework/PageObject_Repo/Loginpage_BM');
var web_common = require('D:/Protractor_Workspace/Protractor_Framework/PageObject_Repo/Web_Common');
var new_customer = require('D:/Protractor_Workspace/Protractor_Framework/PageObject_Repo/NewCustomers');
var create_new_account = require('D:/Protractor_Workspace/Protractor_Framework/PageObject_Repo/Create_New_Account');
var customers = require('D:/Protractor_Workspace/Protractor_Framework/PageObject_Repo/Customers');
var customers_LP = require('D:/Protractor_Workspace/Protractor_Framework/PageObject_Repo/Customer_loginpage');
var reporter = require('D:/Protractor_Workspace/Protractor_Framework/coreframework/Reporter');
var web_utils = require('D:/Protractor_Workspace/Protractor_Framework/coreframework/Web_utils');


/*beforeEach(function() {
	expect(angularHomepage.get()).toEqual(browser.params.title);
})
*/

describe('Create an account for an individual', function() {
  it('Launch bank application and verify home page', function() {
	  if (expect(angularHomepage.get()).toEqual(browser.params.title) 
			  ) {
		  reporter.logteststeps('Home page displayed',false) ;
	} else {
		 reporter.logteststeps('Home page did not displayed',false) ;
	}
  });
  
  it("Bank manager should be able to add an customer", function() {
    angularHomepage.clickOnManagerLogin() ;
    loginpage_BM.clickOnAddCustomer() ;
    new_customer.fillCutomerDetails() ;
  });
  
 it("Bank manager should be able to open an account", function() {
	    loginpage_BM.clickOnOpenAcc() ;
	    create_new_account.createnewacc() ;
	  });
  
  it("New account created should be in database", function() {
	    loginpage_BM.clickOnCustomer() ;
	    customers.verify_Customers_Details(browser.params.fName) ;
	  });
  
  it("Verify Login", function() {
	  web_common.goBacktoHomePage() ;
	  angularHomepage.clickOnCustomerLogin();
	  customers_LP.loginascustomer() ;
	  });
  
});

describe('Do an end to end transaction for a valid customer', function() {
	 	  
	  it("Verify Login", function() {
		  web_common.goBacktoHomePage() ;
		  angularHomepage.clickOnCustomerLogin();
		  customers_LP.loginascustomer() ;
		  });
	  it("Deposite money and verify", function() {
		  	customers_LP.depositeMoney() ;	  
		  });
	  
	  it("Withdraw money and verify", function() {
		  	customers_LP.withdrawmoney() ;
		  });
	  
	});