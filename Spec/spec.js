var angularHomepage = require('D:/Protractor_Workspace/Protractor_Framework/PageObject_Repo/AngularHomepage');
var loginpage_BM = require('D:/Protractor_Workspace/Protractor_Framework/PageObject_Repo/Loginpage_BM');
var web_common = require('D:/Protractor_Workspace/Protractor_Framework/PageObject_Repo/Web_Common');
var new_customer = require('D:/Protractor_Workspace/Protractor_Framework/PageObject_Repo/NewCustomers');
var create_new_account = require('D:/Protractor_Workspace/Protractor_Framework/PageObject_Repo/Create_New_Account');
var customers = require('D:/Protractor_Workspace/Protractor_Framework/PageObject_Repo/Customers');
var customers_LP = require('D:/Protractor_Workspace/Protractor_Framework/PageObject_Repo/Customer_loginpage');



/*beforeEach(function() {
	expect(angularHomepage.get()).toEqual(browser.params.title);
})
*/

describe('Create an account for an individual', function() {
  it('should create an account and verify the account creation', function() {
	//expect(angularHomepage.get()).toEqual('XYZ Bank');
	  expect(angularHomepage.get()).toEqual(browser.params.title);
	  
	 // allure.createStep('Opening main page', function(){})();
	  
	  allure.createStep('Open XYz Bank home page', function () {
	      browser.takeScreenshot().then(function (png) {
	        allure.createAttachment('Main page', function () {return new Buffer(png, 'base64')}, 'image/png')();
	      });
	    })();
  });
  
  it("Bank manager should be able to add an customer", function() {
  	//  angularHomepage.get();
    angularHomepage.clickOnManagerLogin() ;
    loginpage_BM.clickOnAddCustomer() ;
    new_customer.fillCutomerDetails() ;
  });
  
  it("Bank manager should be able to open an account", function() {
	    loginpage_BM.clickOnOpenAcc() ;
	    create_new_account.createnewacc() ;
	  });
  
  it("New account created should be in database", function() {
	    loginpage_BM.clickOnCustomer() ;
	    customers.verify_Customers_Details("Harry") ;
	    //expect(customers.verify_Customers_Details("Harry").toEqual("Harry")) ;
	  });
  
  it("Verify Login", function() {
	  web_common.goBacktoHomePage() ;
	  angularHomepage.clickOnCustomerLogin();
	  customers_LP.loginascustomer() ;
	  
	  });
  
});

/*describe('Do an end to end transaction for a valid customer', function() {
	 	  
	  it("Verify Login", function() {
		  web_common.goBacktoHomePage() ;
		  angularHomepage.clickOnCustomerLogin();
		  customers_LP.loginascustomer() ;
		  });
	  it("Deposite money and verify", function() {
		  	customers_LP.depositeMoney() ;	  
		  });
	  
	  it("Withdraw money and verify", function() {
		  	customers_LP.withdrawmoney() ;
		  });
	  
	  it("Verify the trasaction history", function() {
		  
		  });
	  
	});*/