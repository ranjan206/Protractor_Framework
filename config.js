//var HtmlReporter = require('protractor-beautiful-reporter');

exports.config = {
	framework : 'jasmine2',
	seleniumAddress : 'http://localhost:4444/wd/hub',
	//specs: ['./Spec/spec.js'],
	// specs: ['./Spec/spec_calc.js'],
	//specs: ['./Spec/spec_CreateAccount.js'],

	suites : {
		//bank_manager : './Spec/spec.js',
		bank_manager : './Spec/spec_updated.js',
	},

	params : require('./Testdata.json'),

	capabilities : {
		browserName : 'chrome'
	},

	jasmineNodeOpts : {
		showColors : true, // Use colors in the command line report.
	},

	/*onPrepare : function() {
		// Add a screenshot reporter and store screenshots to `/tmp/screenshots`:
		jasmine
				.getEnv()
				.addReporter(
						new HtmlReporter(
								{
									baseDirectory : 'D:/Protractor_Workspace/Protractor_Framework/Reports/'
								}).getJasmine2Reporter());
	}*/

 onPrepare : function() {
		var AllureReporter = require('D:/Protractor_Workspace/Protractor_Framework/node_modules/jasmine-allure-reporter');
		jasmine
				.getEnv()
				.addReporter(
						new AllureReporter(
								{
									resultsDir : 'D:/Protractor_Workspace/Protractor_Framework/allure-results/'
								}));
	}

/*multiCapabilities: [{
  browserName: 'firefox'
}, {
  browserName: 'chrome'
}]*/
}