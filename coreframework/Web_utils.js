/**
 * <pre>Java script file for all the web interacted methods</pre>
 */

var Web_utils = function() {

  this.acceptAlert = function(alertUrl) {
	  browser.get(alertUrl);
	  browser.pause() ;
	  browser.switchTo().alert().accept() ;
	  debugger ;
		//console.log("Accepted alert message successfully:  "+alertUrl+" \n --- \n"+browser.getCurrentUrl()) ;
};

	this.isElementPresent = function(webElement) {
		var isElementPresent = false ;
		if (webElement.isDisplayed()) {
				isElementPresent = true;
				console.log("Inside isElement present block") ;
			} else {
				isElementPresent = false ;
			}
		return isElementPresent ;
	};

	
	this.clickOnElement = function(webElement) {
		//if (isElementPresent(webElement)
		if (webElement.isDisplayed()
				&& webElement.isEnabled()) {
			webElement.click() ;
		} 
	};
	
	this.selectByValue = function(webElement,valueToBeSelected) {
		
	//	element(by.cssContainingText)
			element(by.cssContainingText(webElement,valueToBeSelected)).click() ;
	};
	
	this.sendkeysByValue = function(webElement,valueToBeFilled) {
		if (webElement.isDisplayed()) {
			webElement.clear();
			webElement.sendKeys(valueToBeFilled) ;
		}
	};
	
	this.verifyTextPresent = function() {
		
	};

};
module.exports = new Web_utils();