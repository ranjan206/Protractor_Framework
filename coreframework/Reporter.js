/**
 * <pre>Java script file for all the reporter</pre>
 */

var Reporter = function() {

	/**
	 * <pre>Logs it in report based on the number of arguments</pre>
	 * @params: Stepdefinition, isScreenshotreq
	 */
	this.logteststeps = function() {
		
			console.log(arguments.length) ;
			console.log(arguments[0]+"---"+arguments[1]) ;
			if (arguments[1]) {
				console.log("Inside if") ;
			}
			capture_screenshot = yes/no
			switch (arguments.length) {
			case 2:
				if (arguments[1]) {
					allure.createStep(arguments[0], function() {
						//new Reporter.captureScreenshots() ;
						browser.takeScreenshot().then(function(png) {
							allure.createAttachment('screenshot', function() {
								return new Buffer(png, 'base64')
							}, 'image/png')();
						});
					})();
				} else {
					allure.createStep(arguments[0], function() {})();
				}
				break;
			default:
				allure.createStep(arguments[0], function() {})();
				break;
			}
	};
	
	
	
	this.captureScreenshots = function() {
		
		browser.takeScreenshot().then(function(png) {
			allure.createAttachment('image', function() {
				return new Buffer(png, 'base64')
			}, 'image/png')();
		});
	};
};

module.exports = new Reporter();