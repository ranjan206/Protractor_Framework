var app = angular.module('reportingApp', []);

app.controller('ScreenshotReportController', function ($scope) {
    $scope.searchSettings = {
        description: '',
        passed: true,
        failed: true,
        pending: true,
        withLog: true,
    };

    $scope.inlineScreenshots = false;
    this.showSmartStackTraceHighlight = true;

    this.chooseAllTypes = function () {
        $scope.searchSettings.passed = true;
        $scope.searchSettings.failed = true;
        $scope.searchSettings.pending = true;
        $scope.searchSettings.withLog = true;
    };

    this.getParent = function (str) {
        var arr = str.split('|');
        str = "";
        for (var i = arr.length - 2; i > 0; i--) {
            str += arr[i] + " > ";
        }
        return str.slice(0, -3);
    };

    this.specLevel = function (str) {
        var arr = str.split('|');
        str = "";
        if (arr.length < 3) {
            return true;
        }
        return false;
    };

    this.getSpec = function (str) {
        return getSpec(str);
    };


    this.getShortDescription = function (str) {
        return str.split('|')[0];
    };


    this.nToBr = function (str) {
        return str.replace(/(?:\r\n|\r|\n)/g, '<br />');
    };


    this.convertTimestamp = function (timestamp) {
        var d = new Date(timestamp),
            yyyy = d.getFullYear(),
            mm = ('0' + (d.getMonth() + 1)).slice(-2),
            dd = ('0' + d.getDate()).slice(-2),
            hh = d.getHours(),
            h = hh,
            min = ('0' + d.getMinutes()).slice(-2),
            ampm = 'AM',
            time;

        if (hh > 12) {
            h = hh - 12;
            ampm = 'PM';
        } else if (hh === 12) {
            h = 12;
            ampm = 'PM';
        } else if (hh == 0) {
            h = 12;
        }

        // ie: 2013-02-18, 8:35 AM
        time = yyyy + '-' + mm + '-' + dd + ', ' + h + ':' + min + ' ' + ampm;

        return time;
    };


    this.round = function (number, roundVal) {
        return (parseFloat(number)/1000).toFixed(roundVal);
    };


    this.passCount = function () {
        var passCount = 0;
        for (var i in this.results) {
            var result = this.results[i];
            if (result.passed) {passCount++};
        }
        return passCount;
    };


    this.pendingCount = function () {
        var pendingCount = 0;
        for (var i in this.results) {
            var result = this.results[i];
            if (result.pending) {pendingCount++};
        }
        return pendingCount;
    };


    this.failCount = function () {
        var failCount = 0;
        for (var i in this.results) {
            var result = this.results[i];
            if (!result.passed && !result.pending) {failCount++}
        }
        return failCount;
    };

    this.applySmartHighlight = function (line) {
        if (this.showSmartStackTraceHighlight) {
            if (line.indexOf('node_modules') > -1) {
                return 'greyout';
            }
            if (line.indexOf('  at ') === -1) {
                return '';
            }

            return 'highlight';
        }
        return true;
    };


    var results =[
    {
        "description": "should create an account and verify the account creation|Create an account for an individual",
        "passed": true,
        "pending": false,
        "os": "Windows NT",
        "sessionId": "7a2849b876477945a6a6319d76d8f193",
        "instanceId": 14436,
        "browser": {
            "name": "chrome",
            "version": "67.0.3396.99"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [],
        "screenShotFile": "00ed001c-00aa-0090-00c1-007300230003.png",
        "timestamp": 1531909387099,
        "duration": 11955
    },
    {
        "description": "Bank manager should be able to add an customer|Create an account for an individual",
        "passed": true,
        "pending": false,
        "os": "Windows NT",
        "sessionId": "7a2849b876477945a6a6319d76d8f193",
        "instanceId": 14436,
        "browser": {
            "name": "chrome",
            "version": "67.0.3396.99"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "007200c1-00cd-0090-0074-000500ee001c.png",
        "timestamp": 1531909399494,
        "duration": 1508
    },
    {
        "description": "Bank manager should be able to open an account|Create an account for an individual",
        "passed": true,
        "pending": false,
        "os": "Windows NT",
        "sessionId": "7a2849b876477945a6a6319d76d8f193",
        "instanceId": 14436,
        "browser": {
            "name": "chrome",
            "version": "67.0.3396.99"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00b6008b-008c-0009-0088-008300ab0019.png",
        "timestamp": 1531909401414,
        "duration": 799
    },
    {
        "description": "New account created should be in database|Create an account for an individual",
        "passed": true,
        "pending": false,
        "os": "Windows NT",
        "sessionId": "7a2849b876477945a6a6319d76d8f193",
        "instanceId": 14436,
        "browser": {
            "name": "chrome",
            "version": "67.0.3396.99"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [],
        "screenShotFile": "00a20023-0027-0024-00fc-000c00e900c7.png",
        "timestamp": 1531909402613,
        "duration": 814
    },
    {
        "description": "Verify Login|Create an account for an individual",
        "passed": true,
        "pending": false,
        "os": "Windows NT",
        "sessionId": "7a2849b876477945a6a6319d76d8f193",
        "instanceId": 14436,
        "browser": {
            "name": "chrome",
            "version": "67.0.3396.99"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [],
        "screenShotFile": "004300df-00a3-0095-0014-00ac0060009d.png",
        "timestamp": 1531909403803,
        "duration": 1627
    },
    {
        "description": "Verify Login|Do an end to end transaction for a valid customer",
        "passed": true,
        "pending": false,
        "os": "Windows NT",
        "sessionId": "7a2849b876477945a6a6319d76d8f193",
        "instanceId": 14436,
        "browser": {
            "name": "chrome",
            "version": "67.0.3396.99"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [],
        "screenShotFile": "00f6005c-006c-005d-00bf-004900dd00ec.png",
        "timestamp": 1531909405790,
        "duration": 713
    },
    {
        "description": "Deposite money and verify|Do an end to end transaction for a valid customer",
        "passed": true,
        "pending": false,
        "os": "Windows NT",
        "sessionId": "7a2849b876477945a6a6319d76d8f193",
        "instanceId": 14436,
        "browser": {
            "name": "chrome",
            "version": "67.0.3396.99"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [],
        "screenShotFile": "00300031-00d5-0019-000e-0095003300ac.png",
        "timestamp": 1531909406873,
        "duration": 835
    },
    {
        "description": "Withdraw money and verify|Do an end to end transaction for a valid customer",
        "passed": true,
        "pending": false,
        "os": "Windows NT",
        "sessionId": "7a2849b876477945a6a6319d76d8f193",
        "instanceId": 14436,
        "browser": {
            "name": "chrome",
            "version": "67.0.3396.99"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [],
        "screenShotFile": "00ea0092-00f8-0091-00e5-00cb00220084.png",
        "timestamp": 1531909408088,
        "duration": 816
    },
    {
        "description": "Verify the trasaction history|Do an end to end transaction for a valid customer",
        "passed": true,
        "pending": false,
        "os": "Windows NT",
        "sessionId": "7a2849b876477945a6a6319d76d8f193",
        "instanceId": 14436,
        "browser": {
            "name": "chrome",
            "version": "67.0.3396.99"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "002f00f9-0077-00ae-002c-0007007d005c.png",
        "timestamp": 1531909409336,
        "duration": 4
    },
    {
        "description": "should create an account and verify the account creation|Create an account for an individual",
        "passed": true,
        "pending": false,
        "os": "Windows NT",
        "sessionId": "e086b463d7e04eed323731750b6373b9",
        "instanceId": 13976,
        "browser": {
            "name": "chrome",
            "version": "67.0.3396.99"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [],
        "screenShotFile": "00ef0054-00c0-00f5-00c3-000c00cf00ce.png",
        "timestamp": 1531911063355,
        "duration": 2996
    },
    {
        "description": "Bank manager should be able to add an customer|Create an account for an individual",
        "passed": true,
        "pending": false,
        "os": "Windows NT",
        "sessionId": "e086b463d7e04eed323731750b6373b9",
        "instanceId": 13976,
        "browser": {
            "name": "chrome",
            "version": "67.0.3396.99"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "005e00ee-001f-0023-0021-00b800b70040.png",
        "timestamp": 1531911066780,
        "duration": 1538
    },
    {
        "description": "Bank manager should be able to open an account|Create an account for an individual",
        "passed": true,
        "pending": false,
        "os": "Windows NT",
        "sessionId": "e086b463d7e04eed323731750b6373b9",
        "instanceId": 13976,
        "browser": {
            "name": "chrome",
            "version": "67.0.3396.99"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00ef008b-009e-007b-0091-000300aa009f.png",
        "timestamp": 1531911068732,
        "duration": 795
    },
    {
        "description": "New account created should be in database|Create an account for an individual",
        "passed": true,
        "pending": false,
        "os": "Windows NT",
        "sessionId": "e086b463d7e04eed323731750b6373b9",
        "instanceId": 13976,
        "browser": {
            "name": "chrome",
            "version": "67.0.3396.99"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [],
        "screenShotFile": "00dc00b2-0016-0035-0081-002700d600a6.png",
        "timestamp": 1531911069914,
        "duration": 1040
    },
    {
        "description": "Verify Login|Create an account for an individual",
        "passed": true,
        "pending": false,
        "os": "Windows NT",
        "sessionId": "e086b463d7e04eed323731750b6373b9",
        "instanceId": 13976,
        "browser": {
            "name": "chrome",
            "version": "67.0.3396.99"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [],
        "screenShotFile": "007000c2-00a9-0024-00d2-00a100ac00ee.png",
        "timestamp": 1531911071316,
        "duration": 1688
    },
    {
        "description": "Verify Login|Do an end to end transaction for a valid customer",
        "passed": true,
        "pending": false,
        "os": "Windows NT",
        "sessionId": "e086b463d7e04eed323731750b6373b9",
        "instanceId": 13976,
        "browser": {
            "name": "chrome",
            "version": "67.0.3396.99"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [],
        "screenShotFile": "00460070-00c4-0075-005d-003f00bf00c6.png",
        "timestamp": 1531911073386,
        "duration": 686
    },
    {
        "description": "Deposite money and verify|Do an end to end transaction for a valid customer",
        "passed": true,
        "pending": false,
        "os": "Windows NT",
        "sessionId": "e086b463d7e04eed323731750b6373b9",
        "instanceId": 13976,
        "browser": {
            "name": "chrome",
            "version": "67.0.3396.99"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [],
        "screenShotFile": "003d007d-0026-0092-00f0-006300920072.png",
        "timestamp": 1531911074440,
        "duration": 839
    },
    {
        "description": "Withdraw money and verify|Do an end to end transaction for a valid customer",
        "passed": true,
        "pending": false,
        "os": "Windows NT",
        "sessionId": "e086b463d7e04eed323731750b6373b9",
        "instanceId": 13976,
        "browser": {
            "name": "chrome",
            "version": "67.0.3396.99"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [],
        "screenShotFile": "008900b5-0085-0050-00ed-0078007e0094.png",
        "timestamp": 1531911075674,
        "duration": 919
    },
    {
        "description": "Verify the trasaction history|Do an end to end transaction for a valid customer",
        "passed": true,
        "pending": false,
        "os": "Windows NT",
        "sessionId": "e086b463d7e04eed323731750b6373b9",
        "instanceId": 13976,
        "browser": {
            "name": "chrome",
            "version": "67.0.3396.99"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00770059-0024-003d-0013-00f000e000b1.png",
        "timestamp": 1531911076992,
        "duration": 4
    }
];

    this.sortSpecs = function () {
        this.results = results.sort(function sortFunction(a, b) {
    if (a.sessionId < b.sessionId) return -1;else if (a.sessionId > b.sessionId) return 1;

    if (a.timestamp < b.timestamp) return -1;else if (a.timestamp > b.timestamp) return 1;

    return 0;
});
    };

    this.sortSpecs();
});

app.filter('bySearchSettings', function () {
    return function (items, searchSettings) {
        var filtered = [];
        var prevItem = null;

        for (var i = 0; i < items.length; i++) {
            var item = items[i];
            item.displaySpecName = false;

            countLogMessages(item);

            var hasLog = searchSettings.withLog && item.browserLogs && item.browserLogs.length > 0;
            if (searchSettings.description === '' ||
                (item.description && item.description.toLowerCase().indexOf(searchSettings.description.toLowerCase()) > -1)) {

                if (searchSettings.passed && item.passed || hasLog) {
                    checkIfShouldDisplaySpecName(prevItem, item);
                    filtered.push(item);
                    var prevItem = item;
                } else if (searchSettings.failed && !item.passed && !item.pending || hasLog) {
                    checkIfShouldDisplaySpecName(prevItem, item);
                    filtered.push(item);
                    var prevItem = item;
                } else if (searchSettings.pending && item.pending || hasLog) {
                    checkIfShouldDisplaySpecName(prevItem, item);
                    filtered.push(item);
                    var prevItem = item;
                }

            }
        }

        return filtered;
    };
});

var checkIfShouldDisplaySpecName = function (prevItem, item) {
    if (!prevItem) {
        item.displaySpecName = true;
        return;
    }

    if (getSpec(item.description) != getSpec(prevItem.description)) {
        item.displaySpecName = true;
        return;
    }
};

var getSpec = function (str) {
    var describes = str.split('|');
    return describes[describes.length-1];
};

var countLogMessages = function (item) {
    if ((!item.logWarnings || !item.logErrors) && item.browserLogs && item.browserLogs.length > 0) {
        item.logWarnings = 0;
        item.logErrors = 0;
        for (var logNumber = 0; logNumber < item.browserLogs.length; logNumber++) {
            var logEntry = item.browserLogs[logNumber];
            if (logEntry.level === 'SEVERE') {
                item.logErrors++;
            }
            if (logEntry.level === 'WARNING') {
                item.logWarnings++;
            }
        }
    }
};